import { MouseEventHandler } from "react";
import styles from "./button.module.css";

type Props = {
	buttonText: string,
	buttonType?: "danger" | "success",
	onClick?: MouseEventHandler<HTMLButtonElement> | undefined,
	href?: string,
}

const Button = ({ buttonText, buttonType, onClick, href }: Props) => {
	return (
		<>
			{!href ?
				<button
					className={`${styles["block-button"]} ${styles[buttonType || ""]}`}
					onClick={onClick}
				>
					{buttonText}
				</button>
			:
				<a className={`${styles["block-button"]} ${buttonType && styles[buttonType]} ${styles["href-block"]}`} href={href}>
					{buttonText}
				</a>
			}
		</>
	)
}

export default Button;