import { useState } from "react";
import styles from "./main.module.css";
import useFetchData from "@/hooks/useFetchData";
import Pagination from "../pagination/Pagination";
import GridBlock from "../gridblock/GridBlock";

const Main = () => {

  const [page, setPage] = useState(1);
  const [pageInput, setPageInput] = useState("" + page);

  const { data, isLoading, error } = useFetchData(page);

  const setPageCounter = async (newPage: number) => {
    if (newPage > 0 && newPage < 5) {
      setPage(newPage);
      setPageInput("" + newPage);
    }
  }

  if (isLoading) {
    return <p>Loading...</p>
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  return (
    <main className={styles.main}>
      <div className={styles["grid-container"]}>
        {data?.map((product, index) =>
          <GridBlock productData={product} key={index}/>
        )}
      </div>
      <Pagination pageInput={pageInput} page={page} setPageCounter={setPageCounter} setPageInput={setPageInput}/>
    </main>
  )
}

export default Main;