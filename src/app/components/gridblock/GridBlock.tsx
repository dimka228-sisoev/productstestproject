/* eslint-disable @next/next/no-img-element */
import { Data } from "@/hooks/useFetchData";
import styles from "./gridblock.module.css";
import Button from "../button/Button";
import Badge from "../badge/Badge";

type Props = {
  productData: Data,
}

const GridBlock = ({ productData }: Props) => {
  return (
    <div className={styles["grid-block"]}>
      <div>
        <img className={styles["product-image"]} src={productData.picture} alt="" width={"100%"} />
        <div className={styles["price-and-badge"]}>
          <div className={styles["product-price"]}>{productData.price} ₽</div>
          <div className={styles["badge-block"]}>
            <Badge badgeText="New" backgroundColor="red" textColor="white" />
            <Badge badgeText="Top" />
          </div>
        </div>
        <div className={styles["product-title"]}>{productData.title}</div>
      </div>
      <div className={styles["product-manufacturer"]}>{productData.manufacturer}</div>
      <Button buttonText="Add to Card" />
    </div>
  )
}

export default GridBlock;