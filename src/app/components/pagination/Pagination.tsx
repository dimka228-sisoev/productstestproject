/* eslint-disable @next/next/no-img-element */
import { SetStateAction } from "react";
import styles from "./pagination.module.css";

type Props = {
  pageInput: string,
  page: number,
  setPageCounter: (newPage: number) => Promise<void>,
  setPageInput: (value: SetStateAction<string>) => void,
}

const Pagination = ({ pageInput, page, setPageCounter, setPageInput }: Props) => {
  return (
    <div className={styles.pagination}>
      <img className={styles["pagination-button"]} src="./left-arrow.png"  alt="" onClick={() => setPageCounter(page - 1)}></img>
      <input value={pageInput} onChange={(e) => setPageInput(e.target.value)} className={styles["page-number-input"]} onKeyDown={(e) => e.key === 'Enter' && setPageCounter(+pageInput)}></input>
      <img className={styles["pagination-button"]} src="./right-arrow.png"  alt="" onClick={() => setPageCounter(page + 1)}></img>
    </div>
  )
}

export default Pagination;