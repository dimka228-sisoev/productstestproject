import styles from "./badge.module.css";
import { Property } from "@/../csstype/index";

type Props = {
  badgeText: string,
  backgroundColor?: Property.BackgroundColor | undefined,
  textColor?: Property.Color | undefined
}

const Badge = ({ badgeText, backgroundColor, textColor }: Props) => {
  return (
    <div style={{ backgroundColor: backgroundColor, color: textColor }} className={styles["badge"]}>
      {badgeText}
    </div>
  )
}

export default Badge;