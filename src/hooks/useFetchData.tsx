import { useEffect, useState } from "react";

export type Data = {
  id: number,
  title: string,
  url: string,
  manufacturer: string,
  picture: string,
  is_hit: boolean,
  price: number,
  discount: number,
}

const useFetchData = ( page: number) => {

  const [data, setData] = useState<Data[]>();
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState<string>();
  
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("./data.json");
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const jsonData: { products: Data[] } = await response.json();
        setData(jsonData.products.slice(6 * (page - 1), 6 * page));
      } catch (err) {
        setError((err as Error).message);
      } finally {
        setLoading(false);
      }
    }

    fetchData();
  }, [page])

  return { data, isLoading, error };
}

export default useFetchData;